var gulp           = require('gulp'),
		sass           = require('gulp-sass'),
		rename         = require('gulp-rename'),
		autoprefixer   = require('gulp-autoprefixer'),
		notify         = require("gulp-notify");



gulp.task('sass', function() {
	return gulp.src('app/scss/**/*.scss')
	.pipe(sass({outputStyle: 'expand', includePaths: require('node-normalize-scss').includePaths}).on("error", notify.onError()))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(gulp.dest('app/css'));

});

gulp.task('watch', ['sass'], function() {
	gulp.watch('app/scss/**/*.scss', ['sass']);
});

gulp.task('default', ['watch']);
