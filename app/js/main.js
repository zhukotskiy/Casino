$(function() {
    let countryCode = null;

    $(document).ready(function(){
        form_validation();
        section_1_fadeIn();
    });

    function form_validation(){
        $.validator.setDefaults({
            success: "valid"
        });

        $("form").each(function (){
            let form = $(this);

            if($('.iti')){
                let input = form.find("input[type=phone]"),
                    iti_el = form.find('.iti.iti--allow-dropdown.iti--separate-dial-code');

                if(iti_el.length){
                    iti.destroy();
                }

                for(var i = 0; i < input.length; i++){
                    iti = intlTelInput(input[i], {
                        autoHideDialCode: false,
                        autoPlaceholder: "aggressive",
                        initialCountry: "auto",
                        preferredCountries: ['ae','us'],
                        customPlaceholder:function(selectedCountryPlaceholder,selectedCountryData){
                            // Change placeholder value on country code change
                            return '+' + selectedCountryData.dialCode;
                        },
                        geoIpLookup: function (success, failure) {
                            // Get user's country code
                            $.get("https://api.ipstack.com/check?access_key=2750e32b97209898592ea58353514fbf", function () { }, "jsonp").always(function (resp) {
                                countryCode = (resp && resp.country_code) ? resp.country_code : "";
                                success(countryCode);
                            });
                        },
                        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.0/js/utils.js"
                    });
                }

                // Set country code value to phone input
                $(document).on('click', '.iti__country-list li[role="option"]', function(){
                    let new_val = $(this).find('.iti__dial-code').text(),
                        tel_input = form.find('input[name="phone"]');

                    tel_input.val(new_val);
                });
            }

            $(this).validate({
                rules: {
                    name: {
                        required: true,
                        minlength: 3
                    },
                    email: {
                        email: true
                    },
                    phone: {
                        required: true,
                        minlength: 9,
                        remote: {
                            url: "https://service.yyy-net.com/phone-validate.php",
                            type: "get",
                            data: {
                                // access_key: 'be988fcfe04eec50ae27a92f19f9a240',
                                number: function () { return form.find("[name='phone']").val() },
                            }
                        }
                    },
                },
                messages: {
                    name: "Invalid name",
                    email: "Invalid email address",
                    phone: "Invalid phone number",
                },
                submitHandler: function(form_data) {
                    //Global flag of form being submitted
                    let isSending = false,
                        loading_block = $(".loading"),
                        loading_spinner =  $('.thumbfan-spinner'),
                        icon_success = $('.success-icon'),
                        icon_error = $('.error-icon');

                    //Check if form is now processed
                    if (isSending) {
                        return;
                    }

                    //Show preloader
                    loading_block.show();

                    //Collect form data
                    let msg = $(form_data).serialize();

                    $.ajax({
                        type: 'POST',
                        url: 'https://land.yyy-net.com/send.php',
                        data: msg,
                        beforeSend: function () {
                            //Set flag that form is being processed
                            isSending = true;
                        },
                        success: function (data) {
                            //Set flag that form has been processed (we recieved data)
                            isSending = false;

                            hide_result_modal();
                            hideSpinner_showIcon('icon_success');
                        },
                        error: function (xhr, str) {
                            //Set flag that form has been processed (we recieved error)
                            isSending = false;

                            hide_result_modal();
                            hideSpinner_showIcon('icon_error');
                        }
                    });

                    // Loading icon animation
                    function hideSpinner_showIcon(value){
                        eval(value).fadeIn('fast');
                        loading_spinner.hide();
                    };

                    // Hide result modal after reponse
                    function hide_result_modal(){
                        setTimeout(function(){
                            loading_block.fadeOut('fast');
                            icon_error.fadeOut('fast');
                            icon_success.fadeOut('fast');
                            $('input').not(':input[type=submit]').val('');
                        },2000);
                    };
                }
            });

            // Set user's country code as value of phone input and set timeout to initialize geoIpLookup() before this func
            setTimeout(function (){
                // Browse all country codes to find user's country code
                form.find('.iti__country').each(function(){
                    if($(this).attr('data-country-code').toUpperCase() === countryCode){
                        $(this).click();
                    }
                });
            },300);
        });
    }


    // Hero block animation
    function section_1_fadeIn(){
        $('.step:eq(0)').show(300, function(){
            $(this).next().show(300, arguments.callee);
        });
    };
});

